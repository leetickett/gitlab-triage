require 'spec_helper'

require 'gitlab/triage/retryable'

describe Gitlab::Triage::Retryable do
  before do
    stub_const('RetryError', Class.new(StandardError))
    stub_const('AnotherRetryError', Class.new(StandardError))
    stub_const('UnexpectedError', Class.new(StandardError))
    stub_const('Retryer', Class.new.include(described_class))
  end

  subject { Retryer.new }

  shared_examples 'exception handling' do
    context 'when an exception is raised' do
      describe 'default behavior' do
        it 'retries StandardError 3 times' do
          subject.execute_with_retry { raise error_to_raise }
        rescue error_to_raise
          expect(subject.tries).to eq(3)
        end
      end

      it 'retries the given exception MAX_RETRIES times' do
        subject.execute_with_retry(exceptions) { raise error_to_raise }
      rescue error_to_raise
        expect(subject.tries).to eq(described_class::MAX_RETRIES)
      end

      it 'raises the final exception' do
        expect do
          subject.execute_with_retry(exceptions) { raise error_to_raise }
        end.to raise_error(error_to_raise)
      end

      it 'ignores unexpected exception' do
        expect do
          subject.execute_with_retry(exceptions) { raise UnexpectedError }
        end.to raise_error(UnexpectedError)
        expect(subject.tries).to eq(1)
      end
    end

    context 'when no exception is raised' do
      it 'returns the result' do
        result = subject.execute_with_retry { 5 }

        expect(result).to eq(5)
      end
    end
  end

  describe '#execute_with_retry' do
    let(:error_to_raise) { RetryError }

    context 'with one possible exception' do
      let(:exceptions) { error_to_raise }

      it_behaves_like 'exception handling'
    end

    context 'with multiple possible exceptions' do
      let(:exceptions) { [error_to_raise, AnotherRetryError] }

      it_behaves_like 'exception handling'

      context 'when second error occurs' do
        let(:error_to_raise) { AnotherRetryError }

        it_behaves_like 'exception handling'
      end
    end
  end
end
