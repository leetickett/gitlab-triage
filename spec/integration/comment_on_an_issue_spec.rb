# frozen_string_literal: true

require 'spec_helper'

describe 'comment on an issue' do
  include_context 'with integration context'

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, state: 'open' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  it 'comments on the issue' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              conditions:
                state: open
              actions:
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
      body: { body: 'This is my comment for issues' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end
end
